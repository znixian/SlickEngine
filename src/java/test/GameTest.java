/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package test;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import xyz.znix.slickengine.BasicState;
import xyz.znix.slickengine.KeyListener;
import xyz.znix.slickengine.MouseListener;
import xyz.znix.slickengine.SlickEngine;
import xyz.znix.slickengine.Context;

/**
 *
 * @author Campbell Suter
 */
public class GameTest {

    public static void main(String[] args) {
        SlickEngine engine = new SlickEngine("This goes on the window titlebar");
        engine.addState(new StartState(engine));
        engine.startFullscreen();
    }

    public static class StartState extends BasicState implements MouseListener {

        public StartState(SlickEngine engine) {
            super(engine);
        }

        @Override
        public void update(Context context, int delta) throws SlickException {
        }

        @Override
        public void render(Context context, Graphics g) throws SlickException {
            g.setBackground(Color.white);
            g.clear();
        }

        @Override
        public void mousePressed(int button, int x, int y) {
            engine.removeState();
            if (button == Input.MOUSE_LEFT_BUTTON) {
                engine.addState(new TestState(engine));
            }
        }

    }

    public static class TestState extends BasicState implements MouseListener, KeyListener {

        int i;

        public TestState(SlickEngine engine) {
            super(engine);
        }

        @Override
        public void update(Context context, int delta) throws SlickException {
//            i += delta;
        }

        @Override
        public void render(Context context, Graphics g) throws SlickException {
            g.setBackground(Color.green);
            g.clear();
            g.setColor(Color.yellow);
            g.fillRect(i, 10, 100, 100);
        }

        @Override
        public void mouseWheelMoved(int change) {
        }

        @Override
        public void mouseClicked(int button, int x, int y, int clickCount) {
        }

        @Override
        public void mousePressed(int button, int x, int y) {
        }

        @Override
        public void mouseReleased(int button, int x, int y) {
        }

        @Override
        public void mouseMoved(int oldx, int oldy, int newx, int newy) {
            i = newx;
        }

        @Override
        public void mouseDragged(int oldx, int oldy, int newx, int newy) {
        }

        @Override
        public void keyPressed(int key, char c) {
            i = 10;
            if (key == Input.KEY_ESCAPE) {
                engine.removeState();
                engine.addState(new StartState(engine));
            }
        }

        @Override
        public void keyReleased(int key, char c) {
        }
    }
}
