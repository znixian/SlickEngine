/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package test;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import xyz.znix.slickengine.networking.ServerAcceptRunnable;
import xyz.znix.slickengine.networking.SimpleSocket;

/**
 *
 * @author Campbell Suter
 */
public class NetworkTest {

	public NetworkTest() throws IOException {
		// server
		ServerSocket server = new ServerSocket();

		// reuse the address. Allows the socket to open without errors.
		server.setReuseAddress(true);

		// bind a port
		server.bind(new InetSocketAddress(4893));

		Thread thread = new Thread(
				new ServerAcceptRunnable(server, this::handleClient));
		thread.start();

		////////////////////////////
		Socket sock = new Socket("localhost", 4893);
		SimpleSocket ss = new SimpleSocket(sock);
		ss.setAction(this::runClient);
		Thread client = new Thread(ss::open);
		client.start();
	}

	private void handleClient(BufferedReader reader,
			BufferedWriter writer) throws IOException {
		writer.append("Hello. What is your name? \n").flush();
		String name = reader.readLine();
		writer.append("Hello, " + name + "\n");
	}

	private void runClient(BufferedReader reader,
			BufferedWriter writer) throws IOException {
		System.out.println("Server said " + reader.readLine());
		String toSend = "Mr. Client.\n";
		System.out.print("Responding: " + toSend);
		writer.append(toSend).flush();
		System.out.println("Server said " + reader.readLine());
	}

	public static void main(String[] args) throws IOException {
		NetworkTest test = new NetworkTest();
	}
}
