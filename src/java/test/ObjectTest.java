/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package test;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import xyz.znix.slickengine.BasicState;
import xyz.znix.slickengine.MouseListener;
import xyz.znix.slickengine.SlickEngine;
import xyz.znix.slickengine.Context;
import xyz.znix.slickengine.objects.BasicObject;
import xyz.znix.slickengine.objects.ObjectList;
import xyz.znix.slickengine.objects.WObject;

/**
 *
 * @author Campbell Suter
 */
public class ObjectTest {

    public static void main(String[] args) {
        SlickEngine engine = new SlickEngine("This goes on the window titlebar");
        engine.addState(new TestState(engine));
        engine.startFullscreen();
    }

    public static class TestState extends BasicState implements MouseListener {

        private final ObjectList objects;
        public static int mouseX, mouseY;

        public TestState(SlickEngine engine) {
            super(engine);
            objects = new ObjectList();

            for (int i = 0; i < 25; i++) {
                objects.add(new Ball());
            }
        }

        @Override
        public void update(Context context, int delta) throws SlickException {
            objects.update(context, delta);
        }

        @Override
        public void render(Context context, Graphics g) throws SlickException {
            g.setBackground(Color.green);
            g.clear();
            objects.render(context, g);
        }

        @Override
        public void mouseMoved(int oldx, int oldy, int newx, int newy) {
            mouseX = newx;
            mouseY = newy;
        }
    }

    public static class Ball extends BasicObject {

        private double xvel, yvel;

        public Ball() {
            x = 50;
            y = 50;

            xvel = (Math.random() * 125) - 2.5;
            yvel = (Math.random() * 25) - 2.5;
        }

        @Override
        public int getWidth() {
            return 0;
        }

        @Override
        public int getHeight() {
            return 0;
        }

        @Override
        public void update(Context context, int delta) throws SlickException {
            x += xvel * delta * 0.01;
            y += yvel * delta * 0.01;

            yvel += 0.05 * delta; // acceleration
            // dammpening
            if (y > context.getHeight() - 25 && yvel > 0) {
                yvel = Math.max(0, yvel - 10);
                yvel = -yvel;
                dampenY();
            }
            if (x > context.getWidth() && xvel > 0) {
                xvel = -xvel;
                dampenX();
            }

            if (y < 0 && yvel < 0) {
                yvel = -yvel;
                dampenY();
            }
            if (x < 0 && xvel < 0) {
                xvel = -xvel;
                dampenX();
            }
        }

        private void dampenX() {
            xvel *= 0.9;
        }

        private void dampenY() {
            yvel *= 0.7;
        }

        @Override
        public void render(Context context, Graphics g) throws SlickException {
            if (isInside(TestState.mouseX, TestState.mouseY)) {
                g.setColor(Color.blue);
                System.out.println("ok!");
            } else {
                g.setColor(Color.yellow);
            }

            g.fillOval(x, y, 50, 50);
        }

        @Override
        public boolean isInside(int x, int y) {
            int tx = getX();
            int ty = getY();

            return WObject.isInside(x, y, tx, ty, 50, 50);
        }

    }
}
