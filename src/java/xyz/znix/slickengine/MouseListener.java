/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package xyz.znix.slickengine;

/**
 * A basic mouse listener. Implement this if you want to process mouse
 * events.
 *
 * @author Campbell Suter
 */
public interface MouseListener extends org.newdawn.slick.MouseListener {

    @Override
    public default void mouseWheelMoved(int change) {
    }

    @Override
    public default void mouseClicked(int button, int x, int y, int clickCount) {
    }

    @Override
    public default void mousePressed(int button, int x, int y) {
    }

    @Override
    public default void mouseReleased(int button, int x, int y) {
    }

    @Override
    public default void mouseMoved(int oldx, int oldy, int newx, int newy) {
    }

    @Override
    public default void mouseDragged(int oldx, int oldy, int newx, int newy) {
    }

}
