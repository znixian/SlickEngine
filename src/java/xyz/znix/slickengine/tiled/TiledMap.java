/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package xyz.znix.slickengine.tiled;

import java.util.Iterator;
import java.util.function.Function;
import org.newdawn.slick.Graphics;

/**
 *
 * @author Campbell Suter
 */
public interface TiledMap {

    default void render(Graphics g) {
        render(g, 0, 0);
    }

    int getTileWidth();

    int getTileHeight();

    int getWidth();

    int getHeight();

    void render(Graphics g, int offsetX, int offsetY);

    void processObjectType(String type, Function<MapObj, Boolean> callback);

    Layer getLayer(int id);

    Layer getLayer(String name);

    Iterator<Layer> getLayers();
}
