/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package xyz.znix.slickengine.impl;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.function.Function;
import org.lwjgl.opengl.Display;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import tiled.core.Map;
import tiled.core.MapLayer;
import tiled.core.MapObject;
import tiled.core.ObjectGroup;
import tiled.core.Tile;
import tiled.core.TileLayer;
import tiled.io.TMXMapReader;
import xyz.znix.slickengine.tiled.Layer;
import xyz.znix.slickengine.tiled.MapObj;
import xyz.znix.slickengine.tiled.TiledMap;

/**
 *
 * @author Campbell Suter
 */
public class TiledMapImpl implements TiledMap {

    private final Map map;
    private ArrayList<Layer> layers;

    public TiledMapImpl(String filename) throws SlickException {
        TMXMapReader reader = new TMXMapReader();
        try {
            map = reader.readMap(filename);
        } catch (Exception ex) {
            throw new SlickException("Exception loading map", ex);
        }
        setup();
    }

    public TiledMapImpl(String filename, InputStream in) throws SlickException {
        TMXMapReader reader = new TMXMapReader();
        try {
            map = reader.readMap(filename, in);
        } catch (Exception ex) {
            throw new SlickException("Exception loading map", ex);
        }
        setup();
    }

    private void setup() {
        layers = new ArrayList<>();
        map.getLayers().forEach((layer) -> {
            if (layer instanceof Layer) {
                layers.add((Layer) layer);
            }
        });
    }

    @Override
    public void processObjectType(String type, Function<MapObj, Boolean> callback) {
        for (MapLayer layer : map) {
            if (layer instanceof ObjectGroup) {
                ObjectGroup objs = (ObjectGroup) layer;
                for (Iterator<MapObject> it = objs.iterator(); it.hasNext();) {
                    MapObject obj = it.next();
                    if (type.equals(obj.getType())) {
                        if (callback.apply(obj)) {
                            it.remove();
                        }
                    }
                }
            }
        }
    }

    public Map getRawMap() {
        return map;
    }

    @Override
    public void render(Graphics g, int offsetX, int offsetY) {
        for (MapLayer layer : map) {
            if (layer instanceof TileLayer) {
                TileLayer tiles = (TileLayer) layer;
                renderLayer(tiles, offsetX, offsetY);
            } else if (layer instanceof ObjectGroup) {
                ObjectGroup objs = (ObjectGroup) layer;
                renderLayer(objs, offsetX, offsetY);
            }
        }
    }

    private void renderLayer(TileLayer tiles, int offsetX, int offsetY) {
        int startTileX = Math.max(0, -offsetX / map.getTileWidth() - 1);
        int startTileY = Math.max(0, -offsetY / map.getTileHeight() - 1);

        int tileWidth = map.getTileWidth();
        int tileHeight = map.getTileHeight();

        int screenSizeX = Display.getWidth() / tileWidth + 3;
        int screenSizeY = Display.getHeight() / tileHeight + 3;

        int maxTilesX = Math.min(startTileX + screenSizeX, tiles.getWidth());
        int maxTilesY = Math.min(startTileY + screenSizeY, tiles.getHeight());

        for (int x = startTileX; x < maxTilesX; x++) {
            for (int y = startTileY; y < maxTilesY; y++) {
                Tile tile = tiles.getTileAt(x, y);
                if (tile != null) {
                    Image img = tile.getImage();
                    img.draw(offsetX + tileWidth * x, offsetY + tileHeight * y);
                }
            }
        }
    }

    private void renderLayer(ObjectGroup objects, int offsetX, int offsetY) {
        for (MapObject obj : objects) {
            Image image = obj.getImage();
            if (image != null) {
                image.draw(offsetX + (int) obj.getX(), offsetY + (int) obj.getY());
            }
            Tile tile = obj.getTile();
            if (tile != null) {
                Image img = tile.getImage();
                img.draw(offsetX + (int) obj.getX(), offsetY + (int) obj.getY());
            }
        }
    }

    @Override
    public int getTileWidth() {
        return map.getTileWidth();
    }

    @Override
    public int getTileHeight() {
        return map.getTileHeight();
    }

    @Override
    public int getWidth() {
        return map.getWidth();
    }

    @Override
    public int getHeight() {
        return map.getHeight();
    }

    @Override
    public Layer getLayer(int id) {
        return layers.get(id);
    }

    @Override
    public Layer getLayer(String name) {
        for (Layer layer : layers) {
            if (name.equals(layer.getName())) {
                return layer;
            }
        }
        return null;
    }

    @Override
    public Iterator<Layer> getLayers() {
        return layers.iterator();
    }
}
