/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package xyz.znix.slickengine.impl;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.Game;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.opengl.InternalTextureLoader;

/**
 *
 * @author Campbell Suter
 */
public class ImprovedAppGameContainer extends AppGameContainer {

    public ImprovedAppGameContainer(Game game) throws SlickException {
        super(game);
    }

    public void setDisplayMode(DisplayMode mode, boolean fullscreen) throws SlickException {
        try {
            targetDisplayMode = mode;
            width = mode.getWidth();
            height = mode.getHeight();

            Display.setDisplayMode(targetDisplayMode);
            Display.setFullscreen(fullscreen);

            if (Display.isCreated()) {
                initGL();
                enterOrtho();
            }

            if (targetDisplayMode.getBitsPerPixel() == 16) {
                InternalTextureLoader.get().set16BitMode();
            }
        } catch (LWJGLException ex) {
            Logger.getLogger(ImprovedAppGameContainer.class.getName()).log(Level.SEVERE, null, ex);
        }

        getDelta();
    }

    @Override
    public void setDisplayMode(int width, int height, boolean fullscreen) throws SlickException {
        if ((this.width == width) && (this.height == height) && (isFullscreen() == fullscreen)) {
            return;
        }

        try {
            targetDisplayMode = null;
            if (fullscreen) {
                DisplayMode[] modes = Display.getAvailableDisplayModes();
                int freq = 0;

                for (DisplayMode current : modes) {
                    if ((current.getWidth() == width) && (current.getHeight() == height)) {
                        if ((targetDisplayMode == null) || (current.getFrequency() >= freq)) {
                            if ((targetDisplayMode == null) || (current.getBitsPerPixel() > targetDisplayMode.getBitsPerPixel())) {
                                targetDisplayMode = current;
                                freq = targetDisplayMode.getFrequency();
                            }
                        }

                        // if we've found a match for bpp and frequence against the 
                        // original display mode then it's probably best to go for this one
                        // since it's most likely compatible with the monitor
                        if ((current.getBitsPerPixel() == originalDisplayMode.getBitsPerPixel())
                                && (current.getFrequency() == originalDisplayMode.getFrequency())) {
                            targetDisplayMode = current;
                            break;
                        }
                    }
                }
            } else {
                targetDisplayMode = new DisplayMode(width, height);
            }

            if (targetDisplayMode == null) {
                throw new SlickException("Failed to find value mode: " + width + "x" + height + " fs=" + fullscreen);
            }

            setDisplayMode(targetDisplayMode, fullscreen);
        } catch (LWJGLException e) {
            throw new SlickException("Unable to setup mode " + width + "x" + height + " fullscreen=" + fullscreen, e);
        }
    }
}
