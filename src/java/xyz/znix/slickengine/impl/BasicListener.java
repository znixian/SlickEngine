/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package xyz.znix.slickengine.impl;

import org.newdawn.slick.ControlledInputReciever;
import org.newdawn.slick.Input;

/**
 *
 * @author Campbell Suter
 */
public interface BasicListener extends ControlledInputReciever {

    @Override
    public default void setInput(Input input) {
    }

    @Override
    public default boolean isAcceptingInput() {
        return true;
    }

    @Override
    public default void inputEnded() {
    }

    @Override
    public default void inputStarted() {
    }

}
