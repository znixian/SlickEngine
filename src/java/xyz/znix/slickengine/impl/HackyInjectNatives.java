/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.slickengine.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.function.Function;

/**
 * Used to inject native libraries into the path at runtime.
 *
 * See
 * http://fahdshariff.blogspot.be/2011/08/changing-java-library-path-at-runtime.html
 */
public class HackyInjectNatives {

    private static boolean isInjected;
    private static final String OS = System.getProperty("os.name").toLowerCase();

    public static void injectNatives() throws IOException, NoSuchFieldException,
            IllegalArgumentException, IllegalAccessException {
        if (isInjected) {
            return;
        }
        isInjected = true;

        String platform = null;

        if (isMac()) {
            platform = "macosx";
        } else if (isSolaris()) {
            platform = "solaris";
        } else if (isUnix()) {
            // saying that linux=unix, as the BSD folders were empty. Sorry.
            platform = "linux";
        } else if (isWindows()) {
            platform = "windows";
        }

        ArrayList<String> files = makeLibList(platform);

        System.out.println("Loading natives for platform: " + platform);

        Path tempPath = Files.createTempDirectory("slickengine-");
        File tempDir = tempPath.toFile();

        System.out.println("making tmp dir at " + tempDir);

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {
                for (File listFile : tempDir.listFiles()) {
                    listFile.delete();
                }
                Files.delete(tempPath);
            } catch (IOException e) {
                e.printStackTrace();
                // too late to fix
            }
        }));

        for (String file : files) {
            extractFile("xyz/znix/slickengine/lwjgl/" + platform + "/" + file, tempDir);
        }

        addLibraryPath(tempDir.getAbsolutePath());

        System.out.println("Done loading natives.");
    }

    /**
     * Adds the specified path to the java library path
     *
     * @param pathToAdd the path to add
     * @throws java.lang.NoSuchFieldException
     * @throws java.lang.IllegalAccessException
     */
    public static void addLibraryPath(String pathToAdd) throws
            NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        final Field usrPathsField = ClassLoader.class.getDeclaredField("usr_paths");
        usrPathsField.setAccessible(true);

        //get array of paths
        final String[] paths = (String[]) usrPathsField.get(null);

        //check if the path to add is already present
        for (String path : paths) {
            if (path.equals(pathToAdd)) {
                return;
            }
        }

        //add the new path
        final String[] newPaths = Arrays.copyOf(paths, paths.length + 1);
        newPaths[newPaths.length - 1] = pathToAdd;
        usrPathsField.set(null, newPaths);
    }

    public static boolean isWindows() {
        return OS.contains("win");
    }

    public static boolean isMac() {
        return OS.contains("mac");
    }

    public static boolean isUnix() {
        return OS.contains("nix") || OS.contains("nux") || OS.indexOf("aix") > 0;
    }

    public static boolean isSolaris() {
        return OS.contains("sunos");
    }

    private static void extractFile(String path, File tempDir) throws IOException {
        String name = path.substring(path.lastIndexOf("/") + 1);

        InputStream ddlStream = HackyInjectNatives.class
                .getClassLoader().getResourceAsStream(path);
        try (FileOutputStream fos = new FileOutputStream(new File(tempDir, name))) {
            byte[] buf = new byte[2048];
            int r = ddlStream.read(buf);
            while (r != -1) {
                fos.write(buf, 0, r);
                r = ddlStream.read(buf);
            }
        }
    }

    private static ArrayList<String> makeLibList(String platform) {
        Function<String, String> process = null;

        switch (platform) {
            case "linux":
            case "solaris":
                process = (in) -> "lib" + in + ".so";
                break;
            case "macosx":
                process = (in) -> (in.equals("openal") ? "" : "lib") + in + ".dylib";
                break;
            case "windows":
                process = (in) -> in + ".dll";
                break;
            default:
                throw new RuntimeException("Unsuported OS: " + platform);
        }

        ArrayList<String> files = new ArrayList<>();

        switch (platform) {
            case "linux":
                files.add(process.apply("jinput-linux"));
                files.add(process.apply("lwjgl"));
                files.add(process.apply("openal"));
                break;
            case "macosx":
                files.add(process.apply("jinput-osx"));
                files.add(process.apply("lwjgl"));
                files.add(process.apply("openal"));
                break;
            case "solaris":
                files.add(process.apply("lwjgl"));
                files.add(process.apply("openal"));
                break;
            case "windows":
                files.add(process.apply("jinput-dx8"));
                files.add(process.apply("jinput-dx8_64"));
                files.add(process.apply("jinput-raw"));
                files.add(process.apply("jinput-raw_64"));
                files.add(process.apply("lwjgl"));
                files.add(process.apply("lwjgl64"));
                files.add(process.apply("OpenAL32"));
                files.add(process.apply("OpenAL64"));
                break;
        }

        // add 64bit versions of solaris and linux libraries
        int num = files.size();
        for (int i = 0; i < num; i++) {
            files.add(files.get(i).replace(".so", "64.so"));
        }

        return files;
    }
}
