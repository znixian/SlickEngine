/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package xyz.znix.slickengine.impl;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.ControllerListener;
import org.newdawn.slick.Game;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.KeyListener;
import org.newdawn.slick.MouseListener;
import org.newdawn.slick.SlickException;
import xyz.znix.slickengine.SlickEngine;
import xyz.znix.slickengine.State;

/**
 *
 * @author Campbell Suter
 */
public class EngineBase implements Game {

    private final SlickEngine engine;
    private AppGameContainer container;
    private final String title;
    private final ContextImpl context;
    private boolean listenersValid;

    public EngineBase(SlickEngine engine, String title) {
        this.engine = engine;
        this.title = title;
        context = new ContextImpl(engine);
    }

    public void setContainer(AppGameContainer container) {
        if (this.container != null) {
            throw new RuntimeException("Container already set");
        }
        this.container = container;
    }

    @Override
    public void init(GameContainer container) throws SlickException {
        applyListeners();
    }

    @Override
    public void update(GameContainer container, int delta) throws SlickException {
        context.setContainer(container);
        engine.getCurrentState().update(context, delta);
        if (!listenersValid) {
            applyListeners();
        }
    }

    @Override
    public void render(GameContainer container, Graphics g) throws SlickException {
        context.setContainer(container);
        engine.getCurrentState().render(context, g);
    }

    @Override
    public boolean closeRequested() {
        return true;
    }

    @Override
    public String getTitle() {
        return title;
    }

    public SlickEngine getEngine() {
        return engine;
    }

    public ContextImpl getContext() {
        return context;
    }

    public void invalidateListeners() {
        listenersValid = false;
    }

    private void applyListeners() {
        listenersValid = true;
        Input input = container.getInput();
        if (input == null) {
            return;
        }
        input.removeAllListeners();
        State state = engine.getCurrentState();
        if (state instanceof MouseListener) {
            input.addMouseListener((MouseListener) state);
        }
        if (state instanceof KeyListener) {
            input.addKeyListener((KeyListener) state);
        }
        if (state instanceof ControllerListener) {
            input.addControllerListener((ControllerListener) state);
        }
    }

}
