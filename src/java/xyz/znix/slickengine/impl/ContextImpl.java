/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package xyz.znix.slickengine.impl;

import org.newdawn.slick.GameContainer;
import xyz.znix.slickengine.AssetLoader;
import xyz.znix.slickengine.Context;
import xyz.znix.slickengine.SlickEngine;

/**
 * Holds all the useful information about the enviroment this game is running
 * in.
 *
 * @author Campbell Suter
 */
public class ContextImpl implements Context {

    private final SlickEngine engine;
    private GameContainer container;

    public ContextImpl(SlickEngine engine) {
        this.engine = engine;
    }

    @Override
    public SlickEngine getEngine() {
        return engine;
    }

    @Override
    public GameContainer getContainer() {
        return container;
    }

    public void setContainer(GameContainer container) {
        this.container = container;
    }

    @Override
    public void drawParent() {

    }

    /**
     * Get the width of the game canvas
     *
     * @return The width of the game canvas
     */
    public int getWidth() {
        return container.getWidth();
    }

    /**
     * Get the height of the game canvas
     *
     * @return The height of the game canvas
     */
    public int getHeight() {
        return container.getHeight();
    }

    @Override
    public AssetLoader getAssetLoader() {
        return null;
    }
}
