/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package xyz.znix.slickengine;

/**
 * Set of utilities for games to use.
 *
 * @author Campbell Suter
 */
public class Utils {

    private static long[] timers;
    private static int level;

    private Utils() {
    }

    public static void startTiming() {
        if (timers == null) {
            timers = new long[10];
        }
        if (level >= timers.length) {
            long[] tmpTimers = new long[timers.length + 10];
            System.arraycopy(timers, 0, tmpTimers, 0, timers.length);
            timers = tmpTimers;
        }
        timers[level] = System.nanoTime();
        level++;
    }

    /**
     * Returns the number of nanoseconds since the corresponding
     * {@link #startTiming() startTiming()} was called.
     *
     * @return
     */
    public static long stopTiming() {
        long stop = System.nanoTime();
        level--;
        long start = timers[level];

        return stop - start;
    }

    /**
     * Prints the number of milliseconds and nanoseconds since the corresponding
     * {@link #startTiming() startTiming()} was called.
     */
    public static void stopTimingAndPrint() {
        long time = stopTiming();

        System.out.println("Took " + (time / 1000000) + "ms, " + (time / 1000) + "us, " + time + "ns");
    }
}
