/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package xyz.znix.slickengine.networking;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;

/**
 * A callback for {@link SimpleSocket}, to be run when the socket is ready.
 *
 * @author Campbell Suter
 */
public interface SocketAction {

	/**
	 * The socket is ready, data can be read and written.
	 *
	 * @param reader
	 * @param writer
	 * @throws IOException
	 */
	public void run(BufferedReader reader, BufferedWriter writer)
			throws IOException;

}
