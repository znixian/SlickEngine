/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package xyz.znix.slickengine.networking;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Listens for connections to a {@link ServerSocket}, running a callback when a
 * client connects.
 *
 * @author Campbell Suter
 */
public class ServerAcceptRunnable implements Runnable {

	private final ServerSocket server;
	private final Consumer<SimpleSocket> handler;

	/**
	 * Create a {@link ServerAcceptRunnable}, that will call the specified
	 * callback when a client connects.
	 *
	 * @param server The server socket to listen on
	 * @param handler The callback to run when a client connects.
	 */
	public ServerAcceptRunnable(ServerSocket server,
			Consumer<SimpleSocket> handler) {
		this.server = server;
		this.handler = handler;
	}

	/**
	 * Create a {@link ServerAcceptRunnable}, that will call the specified
	 * callback when a client connects.
	 *
	 * @param server The server socket to listen on
	 * @param action The callback to run when a client connects.
	 */
	public ServerAcceptRunnable(ServerSocket server,
			SocketAction action) {
		this.server = server;
		this.handler = (socket) -> socket.open(action);
	}

	/**
	 * Run the server acceptor. Note that this is usually run by a thread, and
	 * not usually called manually.
	 */
	@Override
	public void run() {
		try {
			// forever... (accept clients until the 
			//  end of time, or the program stops.)
			while (true) {
				// get a client
				Socket client = server.accept();
				
				// run the callback in a new thread
				Thread thread = new Thread(() -> {
					// run the callback
					handler.accept(new SimpleSocket(client));
				});
				
				// start the new thread
				thread.start();
			}
		} catch (IOException ex) {
			// aww.
			Logger.getLogger(ServerAcceptRunnable.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}
