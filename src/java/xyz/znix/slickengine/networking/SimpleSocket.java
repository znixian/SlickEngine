/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package xyz.znix.slickengine.networking;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Wrapper for a socket. This runs a callback supplying a buffered reader and
 * writer.
 *
 * @author Campbell Suter
 */
public class SimpleSocket {

	private final Socket socket;
	private boolean isOpen;
	private SocketAction action;

	/**
	 * Make a new simple socket, wrapping a plain java socket.
	 *
	 * @param socket The socket to use.
	 */
	public SimpleSocket(Socket socket) {
		this.socket = socket;
	}

	/**
	 * Set the callback to be run, when used with {@link #open() open()}.
	 *
	 * @param action
	 */
	public void setAction(SocketAction action) {
		this.action = action;
	}

	/**
	 * Run the callback supplied with
	 * {@link #setAction(SocketAction) setAction(SocketAction)}, with a reader
	 * and writer for the set socket.
	 */
	public void open() {
		open(action);
	}

	/**
	 * Run the specified callback, with a reader and writer for the set socket.
	 *
	 * @param action The callback to run
	 */
	public void open(SocketAction action) {
		// do not let this socket be opened twice
		if (isOpen) {
			throw new IllegalStateException("Already open!");
		}
		isOpen = true;
		
		// make a buffered reader and writer
		try (BufferedReader reader = new BufferedReader(
				new InputStreamReader(socket.getInputStream()));
				BufferedWriter writer = new BufferedWriter(
						new OutputStreamWriter(socket.getOutputStream()))) {
			
			// actually run stuff
			action.run(reader, writer);
		} catch (IOException ex) {
			// aww.
			Logger.getLogger(SimpleSocket.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}
