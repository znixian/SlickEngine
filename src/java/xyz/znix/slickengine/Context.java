/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package xyz.znix.slickengine;

import org.newdawn.slick.GameContainer;

/**
 * Represents the context that the game is running in (eg, screen size)
 *
 * @author Campbell Suter
 */
public interface Context {

    /**
     * Get the engine running the state
     *
     * @return
     */
    SlickEngine getEngine();

    /**
     * Get the {@link GameContainer}, which provides more detailed information.
     *
     * @return
     */
    GameContainer getContainer();

    /**
     * Get a asset loader that can be used to load certain resources.
     *
     * @return A platform-specific asset loader.
     */
    AssetLoader getAssetLoader();

    /**
     * Draw the parent state. Use this, for example, on pause screens to draw
     * the game in the background.
     */
    void drawParent();

    /**
     * Get the width of the game canvas
     *
     * @return The width of the game canvas
     */
    int getWidth();

    /**
     * Get the height of the game canvas
     *
     * @return The height of the game canvas
     */
    int getHeight();
}
