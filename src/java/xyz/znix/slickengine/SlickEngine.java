/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package xyz.znix.slickengine;

import java.io.IOException;
import java.util.Stack;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.SlickException;
import xyz.znix.slickengine.impl.AutoexitState;
import xyz.znix.slickengine.impl.EngineBase;
import xyz.znix.slickengine.impl.HackyInjectNatives;
import xyz.znix.slickengine.impl.ImprovedAppGameContainer;

/**
 * The game engine itself.
 * <br><br>
 * To run the game, make a instance with {@link #SlickEngine(java.lang.String)
 * SlickEngine()}, then call {@link #startFullscreen() startFullscreen()} or,
 * for more control, {@link #start(boolean, int, int) start()}.
 *
 * @author Campbell Suter
 */
public class SlickEngine {

	private EngineBase engineBase;
	private ImprovedAppGameContainer app;
	private boolean running;
	private final Stack<State> states = new Stack<>();

	/**
	 * Make a new instance of the engine. After this, call
	 * {@link #startFullscreen() startFullscreen()} to start the game.
	 *
	 * @param title The title to put on the window bar
	 */
	public SlickEngine(String title) {
		engineBase = new EngineBase(this, title);

		try {
			HackyInjectNatives.injectNatives();
		} catch (IOException | NoSuchFieldException | IllegalArgumentException | IllegalAccessException ex) {
			throw new RuntimeException(ex);
		}
		try {
			app = new ImprovedAppGameContainer(engineBase);
			engineBase.setContainer(app);
		} catch (SlickException ex) {
			throw new RuntimeException(ex);
		}
	}

	/**
	 * Start the game, so that it starts running, appears on the screen and can
	 * be played. If you want to run the game with known working settings, call
	 * {@link #startFullscreen() startFullscreen()}.
	 *
	 * @param fullscreen Should the game be run in fullscreen mode
	 * @param width The width of the game
	 * @param height The height of the game
	 */
	public void start(boolean fullscreen, int width, int height) {
		init();
		try {
			app.setDisplayMode(width, height, fullscreen);
			// 144 just in case someone has a really fast screen,
			// but still limit the framerate to avoid wasing energy in the GPU.
			app.setTargetFrameRate(144);
			running = true;
			// stops at this point
			app.start();
		} catch (SlickException ex) {
			throw new RuntimeException(ex);
		}
	}

	public void start(boolean fullscreen, DisplayMode mode) {
		init();
		try {
			app.setDisplayMode(mode, fullscreen);
			// 144 just in case someone has a really fast screen,
			// but still limit the framerate to avoid wasing energy in the GPU.
			app.setTargetFrameRate(144);
			running = true;
			// stops at this point
			app.start();
		} catch (SlickException ex) {
			throw new RuntimeException(ex);
		}
	}

	private void init() {
		if (running) {
			throw new RuntimeException("The game was started twice!"
					+ " This should not happen.");
		}
		if (states.empty()) {
			throw new RuntimeException("You must add a state before starting the game!");
		}
		states.add(0, new AutoexitState());
	}

	/**
	 * Start the game, so that it starts running, appears on the screen and can
	 * be played, in full-screen mode at native resolution.
	 */
	public void startFullscreen() {
		DisplayMode best = Display.getDesktopDisplayMode(); //findBestFullscreenDisplayMode();

		System.out.println(best.getWidth() + "x" + best.getHeight() + "x"
				+ best.getBitsPerPixel() + " " + best.getFrequency() + "Hz");

		start(true, best);
		// app.getScreenWidth(), app.getScreenHeight());
	}

	private DisplayMode findBestFullscreenDisplayMode() {
		try {
			DisplayMode[] modes = Display.getAvailableDisplayModes();

			int largestArea = -1;
			DisplayMode largestMode = null;
			for (DisplayMode current : modes) {
				int area = current.getWidth() * current.getHeight();
				if (area > largestArea) {
					largestArea = area;
					largestMode = current;
				}
			}

			return largestMode;
		} catch (LWJGLException ex) {
			throw new RuntimeException(ex);
		}
	}

	/**
	 * Adds a new state to the game, so it is the active state.
	 *
	 * @param item The state to add
	 */
	public void addState(State item) {
		try {
			states.push(item);
			engineBase.getContext().setContainer(app);
			item.init(engineBase.getContext());
			engineBase.invalidateListeners();
		} catch (SlickException ex) {
			throw new RuntimeException("Error initializing state", ex);
		}
	}

	/**
	 * Remove the state at the top of the stack. For example, remove a pause
	 * menu to go back to the game state.
	 *
	 * @return The removed state.
	 */
	public State removeState() {
		State pop = states.pop();
		if (states.empty()) {
			throw new RuntimeException("Cannot remove the last state!");
		} else {
			engineBase.invalidateListeners();
		}
		return pop;
	}

	/**
	 * Get a list of states in the game
	 *
	 * @return The list of states
	 */
	public Stack<State> getStates() {
		return states;
	}

	/**
	 * Find the currently active state.
	 *
	 * @return The currently active state
	 */
	public State getCurrentState() {
		return states.peek();
	}

	public void setShowFPS(boolean showFPS) {
		app.setShowFPS(showFPS);
	}

	/**
	 * Get the base game container for this engine. Please note that you should
	 * only use this when absolutely necessary, as it may change in the future.
	 *
	 * @return
	 */
	public AppGameContainer getApp() {
		return app;
	}
}
