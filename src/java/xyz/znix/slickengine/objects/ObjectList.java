/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package xyz.znix.slickengine.objects;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import xyz.znix.slickengine.Context;

/**
 *
 * @author Campbell Suter
 * @param <T>
 */
public class ObjectList<T extends WObject> implements Iterable<T> {

    private boolean locked;

    protected final List<T> objs;
    private final List<T> toAdd;
    private final List<T> toRemove;

    public void update(Context context, int delta) throws SlickException {
        // lock everything to stop concurrent modification exceptions
        locked = true;
        
        // draw each object
        synchronized (objs) {
            for (WObject obj : objs) {
                obj.update(context, delta);
            }
        }
        
        // unlock the lists
        locked = false;
        
        // apply any changes made while locked
        objs.addAll(toAdd);
        objs.removeAll(toRemove);
        
        // clear the tmp lists
        toAdd.clear();
        toRemove.clear();
    }

    public void render(Context context, Graphics g) throws SlickException {
        synchronized (objs) {
            for (WObject obj : objs) {
                obj.render(context, g);
            }
        }
    }

    public ObjectList() {
        objs = Collections.synchronizedList(new ArrayList<>());
        toAdd = Collections.synchronizedList(new ArrayList<>());
        toRemove = Collections.synchronizedList(new ArrayList<>());
    }

    public int size() {
        return objs.size();
    }

    public boolean isEmpty() {
        return objs.isEmpty();
    }

    public boolean add(T e) {
        if (isLocked()) {
            return toAdd.add(e);
        }
        return objs.add(e);
    }

    public boolean remove(T o) {
        if (isLocked()) {
            return toRemove.add(o);
        }
        return objs.remove(o);
    }

    public void clear() {
        if (isLocked()) {
            toRemove.addAll(objs);
        } else {
            objs.clear();
        }
    }

    @Override
    public Iterator<T> iterator() {
        return objs.iterator();
    }

    public Stream<T> stream() {
        return objs.stream();
    }

    public boolean isLocked() {
        return locked;
    }
}
