/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package xyz.znix.slickengine.objects;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import xyz.znix.slickengine.Context;

/**
 *
 * @author Campbell Suter
 */
public interface WObject {

    int getX();

    int getY();

    void setX(int x);

    void setY(int y);

    int getWidth();

    int getHeight();

    void update(Context context, int delta) throws SlickException;

    void render(Context context, Graphics g) throws SlickException;

    default boolean isInside(int x, int y) {
        int tx = getX();
        int ty = getY();

        return isInside(x, y, tx, ty, getWidth(), getHeight());
    }

    static boolean isInside(int x, int y, int tx, int ty, int width, int height) {
        return x >= tx && y >= ty
                && x < tx + width
                && y < ty + height;
    }

    public default boolean overlaps(WObject other) {
        return overlaps(this, other);
    }

    static boolean overlaps(WObject a, WObject b) {
        return a.getX() < b.getX() + b.getWidth()
                && a.getX() + a.getWidth() > b.getX()
                && a.getY() < b.getY() + b.getHeight()
                && a.getY() + a.getHeight() > b.getY();
    }
}
