/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package xyz.znix.slickengine;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import xyz.znix.slickengine.impl.BasicListener;

/**
 * Represents a state the game can be in. EG, ingame, main menu, pause screen
 * etc.
 *
 * @author Campbell Suter
 */
public interface State extends BasicListener {

    /**
     * Load everything necessary to run the state.
     *
     * @param context The information about the game's enviroment
     * @throws SlickException Throw to indicate an internal error
     */
    default void init(Context context) throws SlickException {
    }

    /**
     * Update everything. This is where things such as movement and AI should be
     * handled. Don't draw stuff here, even though it might work - it is not
     * garunteed to works
     *
     * @param context The information about the game's enviroment
     * @param delta The amount of time thats passed since last update in
     * milliseconds
     * @throws SlickException Throw to indicate an internal error
     */
    void update(Context context, int delta) throws SlickException;

    /**
     * Render everything. This is where you should draw everything. Please note
     * that you should not do any game logic here - see
     * {@link #update(xyz.znix.slickengine.Context, int) update()}
     *
     * @param context The information about the game's enviroment
     * @param g The graphics context that can be used to render.
     * @throws SlickException Throw to indicate a internal error
     */
    void render(Context context, Graphics g) throws SlickException;

}
