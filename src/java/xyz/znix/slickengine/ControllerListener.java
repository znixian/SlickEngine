/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package xyz.znix.slickengine;

/**
 * A basic mouse listener. Implement this if you want to process mouse events.
 *
 * @author Campbell Suter
 */
public interface ControllerListener extends org.newdawn.slick.ControllerListener {

    @Override
    public default void controllerLeftPressed(int controller) {
    }

    @Override
    public default void controllerLeftReleased(int controller) {
    }

    @Override
    public default void controllerRightPressed(int controller) {
    }

    @Override
    public default void controllerRightReleased(int controller) {
    }

    @Override
    public default void controllerUpPressed(int controller) {
    }

    @Override
    public default void controllerUpReleased(int controller) {
    }

    @Override
    public default void controllerDownPressed(int controller) {
    }

    @Override
    public default void controllerDownReleased(int controller) {
    }

    @Override
    public default void controllerButtonPressed(int controller, int button) {
    }

    @Override
    public default void controllerButtonReleased(int controller, int button) {
    }

}
